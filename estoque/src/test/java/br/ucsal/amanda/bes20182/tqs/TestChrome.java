package br.ucsal.amanda.bes20182.tqs;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TestChrome {

	private static WebDriver driver;

	@BeforeClass
	public static void setup() {
		System.setProperty("webdriver.chrome.driver", "C:/Users/amand/OneDrive/�rea de Trabalho/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void testeEstoque() {

		driver.get("http://localhost:8080/estoque/lista-compras.html");

		Select dropdown = new Select(driver.findElement(By.tagName("select")));
		dropdown.selectByVisibleText("Manga");

		WebElement queryInputQuantidade = driver.findElement(By.id("quantidade"));
		queryInputQuantidade.sendKeys("10");

		WebElement queryInputValor = driver.findElement(By.id("valorUnitario"));
		queryInputValor.sendKeys("5");

		WebElement queryInputCaucular = driver.findElement(By.id("calcularBtn"));
		queryInputCaucular.click();

		WebElement conteudo = driver.findElement(By.id("valorTotal"));
		Assert.assertEquals("50", conteudo.getText().toString());
		
		

	}

	
	
}
