package br.ucsal.amanda.bes20182.tqs;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

public class TestFirefox {

	
		private static WebDriver driver;

		@BeforeClass
		public static void setup() {
			System.setProperty("webdriver.gecko.driver","C:/Users/amand/OneDrive/�rea de Trabalho/geckodriver.exe");
			System.setProperty("webdriver.firefox.bin", "C:/Program Files/Mozilla Firefox/firefox.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("marionette", true);
			driver = new FirefoxDriver();

		}

		@Test
		public void testeUcsal() {
			
			driver.get("http://localhost:8080/estoque/lista-compras.html");

			Select dropdown = new Select(driver.findElement(By.tagName("select")));
			dropdown.selectByVisibleText("Manga");

			WebElement queryInputQuantidade = driver.findElement(By.id("quantidade"));
			queryInputQuantidade.sendKeys("10");

			WebElement queryInputValor = driver.findElement(By.id("valorUnitario"));
			queryInputValor.sendKeys("5");

			WebElement queryInputCaucular = driver.findElement(By.id("calcularBtn"));
			queryInputCaucular.click();

			WebElement conteudo = driver.findElement(By.id("valorTotal"));
			Assert.assertEquals("50", conteudo.getText().toString());

		}
		
	}


